export const environment = {
  production: true,
  firebase : {
    apiKey: 'AIzaSyC4UBbK8ecinMnBZP9EodH1IpB5FSDXVu0',
    authDomain: 'studyorb-tech.firebaseapp.com',
    databaseURL: 'https://studyorb-tech.firebaseio.com',
    projectId: 'studyorb-tech',
    storageBucket: 'studyorb-tech.appspot.com',
    messagingSenderId: '498808448943'
  }
};
